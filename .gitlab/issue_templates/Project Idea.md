### Summary

What will this project do? How will it help the "Not Me. Us!" movement?

### Slack link(s)

If this idea comes from the Bernie Slack, add a link to the post(s) and/or channel(s) here.

### Other link(s)

- External docs such as Google Docs and Lucidchart
- Open source databases
- Technical
- Etc.